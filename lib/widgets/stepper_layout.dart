import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_test_app/blocs_cubits/cubit/selected_toggle_index_cubit.dart';
import 'package:flutter_test_app/globals/global_functions.dart';
import 'package:flutter_test_app/widgets/step_widget.dart';

class StepperLayout extends StatelessWidget {
  const StepperLayout({super.key});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return Stack(
          children: [
            BlocBuilder<SelectedToggleIndexCubit, SelectedToggleIndexState>(
              builder: (context, state) {
                if (state is SelectedToggleIndexLoaded) {
                  if (state.selectedIndex == 0) {
                    return const Column(
                      children: [
                        StepWidget(
                          stepNumber: 1,
                          description: 'Erstellen dein Lebenslauf',
                          imagePath: 'svgs/step1.svg',
                        ),
                        // SizedBox(height: 20),
                        StepWidget(
                          stepNumber: 2,
                          description: 'Erstellen dein Lebenslauf',
                          imagePath: 'svgs/step2.svg',
                        ),
                        SizedBox(height: 20),
                        StepWidget(
                          stepNumber: 3,
                          description: 'Mit nur einem Klick bewerben',
                          imagePath: 'svgs/step3.svg',
                        ),
                      ],
                    );
                  } else if (state.selectedIndex == 1) {
                    return const Column(
                      children: [
                        StepWidget(
                          stepNumber: 1,
                          description: 'Erstellen dein Unternehmensprofil',
                          imagePath: 'svgs/step1.svg',
                        ),
                        SizedBox(height: 20),
                        StepWidget(
                          stepNumber: 2,
                          description: 'Erstellen ein Jobinserat',
                          imagePath: 'svgs/undrawaboutme.svg',
                        ),
                        SizedBox(height: 20),
                        StepWidget(
                          stepNumber: 3,
                          description: 'Wähle deinen neuen Mitarbeiter aus',
                          imagePath: 'svgs/undrawswipeprofiles.svg',
                        ),
                      ],
                    );
                  } else if (state.selectedIndex == 2) {
                    return const Column(
                      children: [
                        StepWidget(
                          stepNumber: 1,
                          description: 'Erstellen dein Unternehmensprofil',
                          imagePath: 'svgs/step1.svg',
                        ),
                        SizedBox(height: 20),
                        StepWidget(
                          stepNumber: 2,
                          description:
                              'Erhalte Vermittlungs- angebot von Arbeitgeber',
                          imagePath: 'svgs/undrawjoboffers.svg',
                        ),
                        SizedBox(height: 20),
                        StepWidget(
                          stepNumber: 3,
                          description:
                              'Vermittlung nach Provision oder Stundenlohn',
                          imagePath: 'svgs/undrawbusinessdeal.svg',
                        ),
                      ],
                    );
                  }
                }
                return const SizedBox();
              },
            ),
            if (constraints.maxWidth > desktopThresholdMinWidth)
              Transform.translate(
                offset: const Offset(-70, 155),
                child: Row(
                  children: [
                    const Expanded(child: SizedBox()),
                    SvgPicture.asset(
                      kIsWeb ? 'svgs/arrow1.svg' : 'assets/svgs/arrow1.svg',
                      width: 100,
                      height: 165,
                    ),
                    const Expanded(child: SizedBox()),
                  ],
                ),
              ),
            if (constraints.maxWidth > desktopThresholdMinWidth)
              Transform.translate(
                offset: const Offset(-30, 390),
                child: Row(
                  children: [
                    const Expanded(child: SizedBox()),
                    SvgPicture.asset(
                      'svgs/arrow2.svg',
                      width: 100,
                      height: 180,
                    ),
                    const Expanded(child: SizedBox()),
                  ],
                ),
              ),
          ],
        );
      },
    );
  }

  Offset getPosition(GlobalKey key) {
    RenderBox? box = key.currentContext?.findRenderObject() as RenderBox?;
    if (box != null) {
      return box.localToGlobal(Offset.zero);
    } else {
      return Offset.zero;
    }
  }
}
