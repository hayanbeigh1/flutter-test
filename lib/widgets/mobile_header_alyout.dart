import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MobileHeaderLayout extends StatelessWidget {
  const MobileHeaderLayout({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.lightBlue[50],
      child: Column(
        children: [
          const SizedBox(height: 50),
          const Visibility(
            visible: !kIsWeb,
            child: SizedBox(
              height: 50,
            ),
          ),
          const Padding(
            padding: EdgeInsets.all(16.0),
            child: Column(
              children: [
                Text(
                  'Deine Job website',
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          const SizedBox(height: 20),
          SvgPicture.asset(
            kIsWeb ? 'svgs/handshake.svg' : 'assets/svgs/handshake.svg',
            fit: BoxFit.cover,
            height: 250,
          ),
        ],
      ),
    );
  }
}
