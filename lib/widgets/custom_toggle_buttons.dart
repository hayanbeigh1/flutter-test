import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test_app/blocs_cubits/cubit/selected_toggle_index_cubit.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomToggleButtons extends StatefulWidget {
  const CustomToggleButtons({
    super.key,
  });

  @override
  State<CustomToggleButtons> createState() => _CustomToggleButtonsState();
}

class _CustomToggleButtonsState extends State<CustomToggleButtons> {
  bool toggle1 = true;

  bool toggle2 = false;

  bool toggle3 = false;
  int selectedToggleIndex = 0;
  EdgeInsets edgeInsets =
      const EdgeInsets.symmetric(horizontal: 16.0, vertical: 0);

  var toggleButtonSelectedText = GoogleFonts.lato(
    color: Colors.white,
    fontWeight: FontWeight.bold,
    fontSize: 14,
  );
  var toggleButtonUnselectedText = GoogleFonts.lato(
    color: const Color(0xFF319795),
    fontWeight: FontWeight.bold,
    fontSize: 14,
  );

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 30,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: ToggleButtons(
          fillColor: const Color(0xFF81E6D9),
          selectedColor: const Color(0xFFE6FFFA),
          borderRadius: BorderRadius.circular(10),
          isSelected: [toggle1, toggle2, toggle3],
          borderColor: const Color(0xFFCBD5E0),
          onPressed: (int index) {
            // Handle toggle button selection
            setState(() {
              selectedToggleIndex = index;
            });
            BlocProvider.of<SelectedToggleIndexCubit>(context)
                .setSelectedIndex(index);
            selectActiveToggleButton(index);
          },
          children: <Widget>[
            Padding(
              padding: edgeInsets,
              child: Text(
                'Arbeitnehmer',
                style: getToggleTextStyle(0),
              ),
            ),
            Padding(
              padding: edgeInsets,
              child: Text(
                'Arbeitgeber',
                style: getToggleTextStyle(1),
              ),
            ),
            Padding(
              padding: edgeInsets,
              child: Text(
                'Transporteure',
                style: getToggleTextStyle(2),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void selectActiveToggleButton(int index) {
    if (index == 0) {
      setState(() {
        toggle1 = true;
        toggle2 = false;
        toggle3 = false;
      });
    }
    if (index == 1) {
      setState(() {
        toggle1 = false;
        toggle2 = true;
        toggle3 = false;
      });
    }
    if (index == 2) {
      setState(() {
        toggle1 = false;
        toggle2 = false;
        toggle3 = true;
      });
    }
  }

  TextStyle getToggleTextStyle(int index) {
    if (selectedToggleIndex == index) {
      return toggleButtonSelectedText;
    } else {
      return toggleButtonUnselectedText;
    }
  }
}
