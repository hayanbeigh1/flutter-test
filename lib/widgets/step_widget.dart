import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_test_app/globals/global_functions.dart';
import 'package:flutter_test_app/utils/clippers.dart';
import 'package:google_fonts/google_fonts.dart';

class StepWidget extends StatefulWidget {
  final int stepNumber;
  final String description;
  final String imagePath;

  const StepWidget({
    super.key,
    required this.stepNumber,
    required this.description,
    required this.imagePath,
  });

  @override
  State<StepWidget> createState() => _StepWidgetState();
}

class _StepWidgetState extends State<StepWidget> {
  final GlobalKey textKey = GlobalKey();
  final GlobalKey containerKey = GlobalKey();
  Offset? textPosition;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _getTextPosition();
    });
  }

  @override
  void didUpdateWidget(covariant StepWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _getTextPosition();
    });
  }

  void _getTextPosition() {
    final RenderBox? textRenderBox =
        textKey.currentContext?.findRenderObject() as RenderBox?;
    final RenderBox? containerRenderBox =
        containerKey.currentContext?.findRenderObject() as RenderBox?;
    if (textRenderBox != null && containerRenderBox != null) {
      final Offset globalPosition = textRenderBox.localToGlobal(Offset.zero);
      final Offset localPosition =
          containerRenderBox.globalToLocal(globalPosition);
      setState(() {
        textPosition = localPosition;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: widget.stepNumber == 2 ? StepClip() : null,
      child: Container(
        decoration: widget.stepNumber == 2
            ? const BoxDecoration(
                gradient: LinearGradient(
                  colors: [Color(0xFFEBF4FF), Color(0xFFE6FFFA)],
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                ),
              )
            : null,
        padding: const EdgeInsets.symmetric(vertical: 40.0),
        child: LayoutBuilder(builder: (context, constraints) {
          final bool isDesktop =
              constraints.maxWidth > desktopThresholdMinWidth;
          return isDesktop ? _buildRowLayout() : _buildColumnLayout();
        }),
      ),
    );
  }

  Widget _buildRowLayout() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _getTextPosition();
    });
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.center,
      children:
          widget.stepNumber == 2 ? getInnerRow.reversed.toList() : getInnerRow,
    );
  }

  Widget _buildColumnLayout() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _getTextPosition();
    });
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: widget.stepNumber == 1
          ? getInnerColumn.reversed.toList()
          : getInnerColumn,
    );
  }

  List<Widget> get getInnerColumn {
    return [
      Stack(
        key: containerKey,
        children: [
          if (textPosition != null && widget.stepNumber == 1)
            _buildCircle(textPosition!, const Offset(-200, -80)),
          if (textPosition != null && widget.stepNumber == 3)
            _buildCircle(textPosition!, const Offset(-200, -80)),
          Column(
            children: widget.stepNumber == 1
                ? getStepperContent
                : getStepperContent.reversed.toList(),
          ),
          Positioned(
            bottom: widget.stepNumber != 1 ? 20 : null,
            left: 0,
            right: 0,
            child: SvgPicture.asset(
              !kIsWeb ? 'assets/${widget.imagePath}' : widget.imagePath,
              height: 170,
            ),
          ),
        ],
      ),
    ];
  }

  Widget _buildCircle(Offset position, Offset offset) {
    return Transform.translate(
      offset: position + offset,
      child: Container(
        padding: const EdgeInsets.all(100),
        decoration: const BoxDecoration(
          color: Color(0xFFF7FAFC),
          shape: BoxShape.circle,
        ),
      ),
    );
  }

  List<Widget> get getStepperContent {
    return [
      Container(
        height: widget.stepNumber == 2 ? 160 : 150,
        color: Colors.transparent,
      ),
      Container(
        padding: const EdgeInsets.symmetric(vertical: 50),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            const SizedBox(width: 20),
            Text(
              key: textKey,
              '${widget.stepNumber}.',
              style: GoogleFonts.lato(
                color: const Color(0xFF718096),
                fontSize: 60,
                height: 0.8,
              ),
            ),
            const SizedBox(width: 10),
            SizedBox(
              width: 170,
              child: Text(
                widget.description,
                style: GoogleFonts.lato(
                  fontSize: 14,
                  color: const Color(0xFF718096),
                  height: 1,
                ),
              ),
            ),
          ],
        ),
      ),
    ];
  }

  List<Widget> get getInnerRow {
    return [
      Expanded(
        child: Stack(
          key: containerKey,
          children: [
            if (textPosition != null && widget.stepNumber != 2)
              Positioned(
                left: textPosition!.dx - 20,
                top: textPosition!.dy - 10,
                child: Container(
                  width: 80,
                  height: 80,
                  decoration: const BoxDecoration(
                    color: Color(0xFFF7FAFC),
                    shape: BoxShape.circle,
                  ),
                ),
              ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 50),
              child: Row(
                mainAxisAlignment: widget.stepNumber == 2
                    ? MainAxisAlignment.start
                    : MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  const SizedBox(width: 20),
                  Text(
                    key: textKey,
                    '${widget.stepNumber}.',
                    style: GoogleFonts.lato(
                      color: const Color(0xFF718096),
                      fontSize: 60,
                      height: 0.8,
                    ),
                  ),
                  const SizedBox(width: 10),
                  SizedBox(
                    width: 170,
                    child: Text(
                      widget.description,
                      style: GoogleFonts.lato(
                        fontSize: 14,
                        color: const Color(0xFF718096),
                        height: 1,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      const SizedBox(width: 20),
      Expanded(
        child: Align(
          alignment: widget.stepNumber == 2
              ? Alignment.centerRight
              : Alignment.centerLeft,
          child: SvgPicture.asset(
            kIsWeb ? widget.imagePath : 'assets/${widget.imagePath}',
            height: 100,
          ),
        ),
      ),
    ];
  }
}
