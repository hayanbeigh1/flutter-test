import 'package:flutter/material.dart';

isMobileView({required BuildContext context}) {
  double windowWidth = MediaQuery.of(context).size.width;
  if (windowWidth > 700) {
    return false;
  }
  return true;
}

const desktopThresholdMinWidth = 700;
