import 'package:flutter/material.dart';

class StepClip extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();

    path.lineTo(0, 0);

    path.lineTo(0, size.height - 30);

    path.quadraticBezierTo(
      size.width * 0.35,
      size.height - 40,
      size.width * 0.45,
      size.height - 40,
    );

    path.quadraticBezierTo(
      size.width * 0.9,
      size.height - 20,
      size.width,
      size.height - 100,
    );

    path.lineTo(size.width, 60);

    path.quadraticBezierTo(
      size.width * 0.95,
      0,
      size.width * 0.6,
      30,
    );
    path.quadraticBezierTo(
      size.width * 0.45,
      50,
      size.width * 0.3,
      30,
    );
    path.quadraticBezierTo(
      size.width * 0.1,
      0,
      0,
      20,
    );

    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

class WavyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0, size.height); // Start the path at the bottom left

    var firstControlPoint = Offset(size.width / 4, size.height);
    var firstEndPoint = Offset(size.width / 2, size.height - 20);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);

    var secondControlPoint = Offset(size.width * 3 / 4, size.height - 30);
    var secondEndPoint = Offset(size.width, size.height - 30);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);

    path.lineTo(
        size.width, size.height - 30); // Draw to the bottom right corner
    path.lineTo(size.width, 0); // Draw to the top right corner
    path.close(); // Close the path

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false; // No need to reclip
  }
}
