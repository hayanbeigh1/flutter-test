import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

part 'selected_toggle_index_state.dart';

class SelectedToggleIndexCubit extends Cubit<SelectedToggleIndexState> {
  SelectedToggleIndexCubit() : super(SelectedToggleIndexInitial());

  setSelectedIndex(int index) {
    emit(SelectedToggleIndexLoading());
    emit(SelectedToggleIndexLoaded(index));
  }
}
