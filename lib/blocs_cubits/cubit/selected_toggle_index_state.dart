part of 'selected_toggle_index_cubit.dart';

@immutable
sealed class SelectedToggleIndexState {}

final class SelectedToggleIndexInitial extends SelectedToggleIndexState {}

final class SelectedToggleIndexLoading extends SelectedToggleIndexState {}

final class SelectedToggleIndexLoaded extends SelectedToggleIndexState {
  final int selectedIndex;

  SelectedToggleIndexLoaded(this.selectedIndex);
}
